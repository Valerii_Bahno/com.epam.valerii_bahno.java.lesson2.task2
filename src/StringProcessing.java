import java.util.Scanner;

public class StringProcessing {
    public static void main(String[] args) {
        StringProcessing stringProcess = new StringProcessing();
        stringProcess.StringProcess();
    }

    public void StringProcess() {
        /*Option 1 - Input n lines from the console, find the shortest and longest line.
        Show found lines and their length. */
        Scanner in = new Scanner(System.in);

        /*We declare an array of, for example, 5 lines and enter them from the console*/
        String[] mas = new String[5];
        System.out.println("Введите строки:");
        mas[0] = in.nextLine();
        mas[1] = in.nextLine();
        mas[2] = in.nextLine();
        mas[3] = in.nextLine();
        mas[4] = in.nextLine();

        /*Using a for loop, iterate over the lengths of the array elements and find the shortest and longest strings, 
        and print them and their lengths to the console*/
        int min = 1;
        int max = 0;
        String stringMin = "";
        String stringMax = "";
        for(int i = 0; i < mas.length; i++) {
            if (mas[i].length() <= min) {
                min = mas[i].length();
                stringMin = mas[i];
            }
        }
        System.out.println("Shortest line: " + stringMin + ". Its length: " + min);

        for(int i = 0; i < mas.length; i++) {
            if (mas[i].length() > max) {
                max = mas[i].length();
                stringMax = mas[i];
            }
        }
        System.out.println("Longest line: " + stringMax + ". Its length: " + max);
    }
}